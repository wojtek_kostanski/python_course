class Account:
  def __init__(self,file_path):
    self.file_path = file_path
    with open(file_path, 'r') as file:
      self.balance = int(file.read())

  def withdraw(self,amount):
    self.balance = self.balance - amount

  def deposit(self,amount):
    self.balance = self.balance + amount

  def commit(self):
    with open(self.file_path, 'w') as file:
      file.write(str(self.balance))

account = Account("balance.txt")
print(account.balance)
account.commit()